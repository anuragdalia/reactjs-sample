import Head from 'next/head'
import styles from '../styles/Home.module.css'
import {CheckBox, EditText, Form, RadioGroup, SubmitButton} from "../components/Form";
import {throwStatement} from "@babel/types";

export default function Home() {

    function onFormSubmit(values) {
        console.log(JSON.stringify(values))
    }

    function validateUserName(v) {
        if (v.match(/^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/)) {
            return v;
        } else {
            throw Error("doesnt match shit")
        }
    }

    return (
        <div className={styles.container}>
            <Head>
                <title>Create Next App</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <main className={styles.main}>
                <Form onSubmit={onFormSubmit}>
                    <EditText label={"Username"} name={"username"}
                              validate={validateUserName} validateOnChange={true}/>
                    <EditText label={"Email"} name={"email"}
                              validate={(v) => v}/>
                    <CheckBox label={"Happy?"} name={"isHappy"}
                              validate={(v) => v}
                              value={"happy"}/>
                    <RadioGroup label={"Male"} name={"gender"}
                                value={"male"}
                                validate={(v) => v}/>
                    <RadioGroup label={"Female"} name={"gender"}
                                value={"female"}
                                validate={(v) => v}/>
                    <SubmitButton name={"submit"}
                                  value={"Submit"}
                                  validate={(v) => v}/>
                </Form>
            </main>

            <footer className={styles.footer}>
                <a
                    href="http://baapkicompany.com"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Powered by{' '}
                    Dalia
                </a>
            </footer>
        </div>
    )
}
