import React from "react";

interface StandardInputImpl {
    name: string,
    value?: any,
    label?: any,
    validate: (state) => any,
    onChange?: (event) => any,
    validateOnChange?: boolean,
    onFieldChange?: (name, value) => any
}

interface AppProps {
    children: React.ReactElement<StandardInputImpl>[];
    onSubmit?: (values) => any;
}

export class Form extends React.Component<AppProps, any> {
    constructor(props: Readonly<AppProps> | AppProps) {
        super(props);
        this.state = {}
    }

    onSubmit(event) {
        const theForm = this;
        const endResult = this.props.children.reduce((acc, item) => {
            if (item && item.props.name && item.props.name !== "submit") {
                try {
                    acc[item.props.name] = item.props.validate(theForm.state[item.props.name])
                } catch (e) {
                    this.setState({error: e.message})
                }
            }
            return acc;
        }, {});
        this.props.onSubmit(endResult)
        event.preventDefault()
        return false;
    }

    onFieldChange(fieldName, value) {
        const abc = {}
        abc[fieldName] = value
        this.setState(() => abc)
    }

    render() {
        return <>
            <form onSubmit={e => this.onSubmit(e)}>
                {this.props.children.map((ele, index) => React.cloneElement(ele, {
                    key: `${ele.props.name}_${index}`,
                    onFieldChange: (n, v) => this.onFieldChange(n, v)
                }))}
            </form>
            <br/>
            <div>{this.state.error}</div>
        </>;
    }
}

abstract class Input<P extends StandardInputImpl> extends React.Component<P, { error: string, value: string }> {
    protected constructor(props: Readonly<P> | P) {
        super(props);
        this.state = {value: this.props.value || "", error: ""};
    }

    abstract getType(): string

    onClick(e) {
    }

    onChange(event) {
        let value;
        switch (this.getType()) {
            case "text":
                value = event.target.value;
                break;
            case "radio":
                if (event.target.checked)
                    value = event.target.value
                else
                    value = undefined
                break;
            case "submit":
                value = "submit"
                break;
            case "checkbox":
                if (event.target.checked)
                    value = event.target.value
                else
                    value = undefined
                break;
            default:
                throw Error("unhandled")
        }

        this.setState({value: value})
        this.props?.onChange?.call(null, {value: value})
        this.props?.onFieldChange?.call(null, this.props.name, value)

        if (!!this.props?.validateOnChange) {
            try {
                this.props.validate(value)
                this.setState({error: undefined})
            } catch (e) {
                this.setState({error: e.message})
            }
        }
    }

    render() {
        return <div style={this.state.error ? {"border": "1px solid red"} : {}}>{this.props?.label && <>
            <label>{this.props.label} {this.state.error}</label><br/></>}
            <input
                onClick={e => this.onClick(e)}
                name={this.props.name}
                value={this.state.value}
                onChange={e => this.onChange(e)}
                type={this.getType()}/></div>;
    }

}

export class EditText extends Input<StandardInputImpl> {
    constructor(props: Readonly<StandardInputImpl> | StandardInputImpl) {
        super(props);
    }

    getState() {
        return this.state
    }

    getType() {
        return "text"
    }
}

export class SubmitButton extends Input<StandardInputImpl> {
    constructor(props: Readonly<StandardInputImpl> | StandardInputImpl) {
        super(props);
    }

    getType(): string {
        return "submit";
    }
}

export class CheckBox extends Input<StandardInputImpl> {
    constructor(props: Readonly<StandardInputImpl> | StandardInputImpl) {
        super(props);
    }

    getType(): string {
        return "checkbox";
    }
}

export class RadioGroup extends Input<StandardInputImpl> {

    constructor(props: Readonly<StandardInputImpl> | StandardInputImpl) {
        super(props);
    }

    getType(): string {
        return "radio";
    }
}